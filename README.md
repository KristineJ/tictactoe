# Super simple Tic Tac Toe game #

The game is made with C++ and SFML 2.3.2 in Visual Studio 2015.

Player can choose board size, currently restricted to 9x9 squares, but the code would allow for something bigger with minor changes to texture size.

I used this tutorial to work from, but the check win condition is pretty much the only thing I didn't really change.

https://darknessleftstudio.wordpress.com/2015/10/25/tic-tac-toe-1-opening-up-a-sfml-window/

#TO DO:#
 * Add start menu for choosing board size (currently using console window for this)
 * Add replay and end game buttons
 * Add rules to AI so it actually tries to win (maybe use Minimax algorithm)