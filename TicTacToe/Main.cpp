#include <iostream>
#include "SFML\Graphics.hpp"
#include "Board.h"
#include "InputHandler.h"
#include "Computer.h"
#include <time.h>

// Gets a number from the user and returns it
int getUserInput()
{
	int input;

	std::cout << "Please select board size (3, 6 or 9): ";
	std::cin >> input;

	return input;
}

int main() 
{
	int userInput;
	int boardSize;
	int player = 1;
	float xPos = 0;
	float yPos = 0;
	bool win = false;
	bool draw = false;

	srand(time(NULL));

	// Get user to choose board size
	userInput = getUserInput();

	while (userInput % 3 != 0 || userInput > MAX_BOARD_SIZE)
	{
		userInput = getUserInput();
	}

	boardSize = userInput;

	Board board(boardSize);
	InputHandler inputHandler(boardSize);
	Computer computer;

	if (boardSize != 0) 
	{

		//Initialize window
		sf::RenderWindow window(sf::VideoMode(board.boardWidth, board.boardHeight), "TIC TAC TOE");

		//Handle events while window is open
		while (window.isOpen())
		{
			sf::Event event;

			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
				{
					window.close();
				}

				if (!win && !draw)
				{
					if (player == 1)
					{
						if (computer.computerMove(board, player))
						{
							win = board.check_win(player);
							draw = board.checkDraw();

							if (!win && !draw)
							{
								player = 2;
							}
						}
					}

					if (player == 2)
					{
						if (event.type == sf::Event::MouseButtonPressed)
						{

							xPos = sf::Mouse::getPosition(window).x;
							yPos = sf::Mouse::getPosition(window).y;

							// Check if valid position and switch to next player if true
							if (inputHandler.handleMouseClick(board, player, xPos, yPos))
							{
								win = board.check_win(player);
								draw = board.checkDraw();

								if (!win && !draw)
								{
									player = 1;
								}
							}
						}
					}




				}
			}

			// Draw to window and display it
			board.drawBoard(window);

			if (win)
			{
				board.drawWinScene(window, player);
			}

			if (draw)
			{
				board.drawEndScene(window);
			}


			window.display();

		}
	}

	return 0;
}

