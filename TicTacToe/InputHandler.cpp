#include "InputHandler.h"

// Constructor that takes users board size as parameter
InputHandler::InputHandler(int size)
{
	boardSize = size;
}

// Handles mouse clicks and sets score
bool InputHandler::handleMouseClick(Board &board, int player, float x, float y)
{
	int row;
	int col;
	bool foundPosX = false;
	bool foundPosY = false;

	// Find X-position
	for (int i = 0; i < boardSize; i++)
	{
		if (x < board.squareSize * (i + 1) && x > board.squareSize * i)
		{
			row = i;
			foundPosX = true;
		}
	}

	// Find Y-position
	for (int j = 0; j < boardSize; j++)
	{

		if (y < board.squareSize * (j + 1) && y > board.squareSize * j)
		{
			col = j;
			foundPosY = true;
		}
	}

	// If position is found, set score and return true
	if (foundPosX && foundPosY) {

		if (board.matrix[row][col] == 0)
		{
			board.setScore(player, row, col, x, y);
			return true;
		}

		else
		{
			std::cout << "This spot is already taken! Choose a valid spot." << std::endl;
		}
	}

	return false;
}
