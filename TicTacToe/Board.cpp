#include "Board.h"

// Constructor that takes users board size as parameter
Board::Board(int size) 
{
	boardSize = size;
	setSquareSize(boardSize);

	boardHeight = boardSize*squareSize;
	boardWidth = boardHeight;

	circleSpriteNumber = 0;
	crossSpriteNumber = 0;

	matrix.resize(boardSize, std::vector<int>(boardSize, 0));
	circleSprites.resize(boardSize*boardSize);
	crossSprites.resize(boardSize*boardSize);

	loadTextures(boardSize);

}

// Draw board
void Board::drawBoard(sf::RenderWindow &window) 
{
	window.clear(sf::Color::Black);

	std::vector<sf::RectangleShape> horizontalLines(boardSize);
	std::vector<sf::RectangleShape> verticalLines(boardSize);

	// Set up lines for the board
	for (int i = 0; i < boardSize; i++) 
	{
		horizontalLines[i] = sf::RectangleShape (sf::Vector2f(boardWidth, LINE_SIZE_GRID));
		horizontalLines[i].setFillColor(sf::Color::White);
		horizontalLines[i].setPosition(0, squareSize*(i + 1));

		verticalLines[i] = sf::RectangleShape(sf::Vector2f(LINE_SIZE_GRID, boardHeight));
		verticalLines[i].setFillColor(sf::Color::White);
		verticalLines[i].setPosition(squareSize*(i + 1), 0);
	}

	// Draw lines to window
	for (int i = 0; i < boardSize; i++)
	{
		window.draw(horizontalLines[i]);
		window.draw(verticalLines[i]);
	}

	// Draw circle sprites
	if (circleSpriteNumber != 0)
	{
		for (int i = 1; i <= circleSpriteNumber; i++)
		{
			window.draw(circleSprites[i]);
		}
	}

	// Draw cross sprites
	if (crossSpriteNumber != 0)
	{
		for (int i = 1; i <= crossSpriteNumber; i++)
		{
			window.draw(crossSprites[i]);
		}
	}
}

// Sets the score in the boardmatrix and position for sprites
void Board::setScore(int player, int row, int col, int xPos, int yPos)
{
	matrix[row][col] = player;

	if (player == 1)
	{
		circleSpriteNumber++;
		circleSprites[circleSpriteNumber].setPosition((xPos - (xPos % squareSize)) + SQUARE_OFFSET, (yPos - (yPos % squareSize)) + SQUARE_OFFSET);
	}

	else if (player == 2)
	{
		crossSpriteNumber++;
		crossSprites[crossSpriteNumber].setPosition((xPos - (xPos % squareSize)) + SQUARE_OFFSET, (yPos - (yPos % squareSize)) + SQUARE_OFFSET);
	}
}

// Check if a player has won the game
bool Board::check_win(int player)
{
	int count;

	// Check horizontally
	for (int i = 0; i < boardSize; ++i)
	{
		count = 0;

		for (int j = 0; j < boardSize; ++j)
		{
			if (matrix[i][j] == player)
				count++;
		}

		if (count == boardSize)
		{
			return true;
		}
	}

	// Check vertically
	for (int j = 0; j < boardSize; ++j)
	{
		count = 0;

		for (int i = 0; i < boardSize; ++i)
		{
			if (matrix[i][j] == player)
				count++;
		}

		if (count == boardSize)
		{
			return true;
		}
	}

	// Check diagonally
	count = 0;

	for (int i = 0, j = 0; i < boardSize; ++i, ++j)
	{
		if (matrix[i][j] == player)
			count++;
	}

	if (count == boardSize)
	{
		return true;
	}

	count = 0;

	for (int i = 0, j = boardSize-1; i < boardSize; ++i, --j)
	{
		if (matrix[i][j] == player)
			count++;
	}

	if (count == boardSize)
	{
		return true;
	}

	return false;
}

// Draws win text to the window
void Board::drawWinScene(sf::RenderWindow &window, int player)
{
	if (player == 1)
	{
		text.setString("O wins");
	}
	else
	{
		text.setString("X wins");
	}

	window.draw(text);
}

// Check if it's a draw
bool Board::checkDraw()
{
	int fullBoard = 0;

	for (int i = 0; i < boardSize; i++)
	{
		for (int j = 0; j < boardSize; j++)
		{
			if (matrix[i][j] != 0)
			{
				fullBoard++;
			}
		}
	}

	if (fullBoard == boardSize*boardSize)
	{
		return true;
	}

	return false;
}

// Draws text to the window if a draw happens
void Board::drawEndScene(sf::RenderWindow &window)
{
	text.setString("Draw!");
	window.draw(text);
}

// Load textures for sprites based on board size, sets text/font
void Board::loadTextures(int size)
{

	font.loadFromFile("Fonts/BADABB.ttf");
	text.setFont(font);
	text.setCharacterSize(150);
	text.setColor(sf::Color::Cyan);
	text.setPosition(boardSize*squareSize / 5, boardSize*squareSize / 3);

	if (size == 3)
	{
		circleTexture.loadFromFile("Images/o-shape-200.png");
		crossTexture.loadFromFile("Images/x-shape-200.png");
	}

	else
	{
		circleTexture.loadFromFile("Images/o-shape-100.png");
		crossTexture.loadFromFile("Images/x-shape-100.png");
	}


	for (int i = 0; i < size*size; i++)
	{
		circleSprites[i].setTexture(circleTexture);
		crossSprites[i].setTexture(crossTexture);
	}
}

// Set size for squares on the board
void Board::setSquareSize(int size)
{
	if (size == 3)
	{
		squareSize = 200;
	}

	else
	{
		squareSize = 100;
	}

}
