#pragma once
#include "Board.h"

class InputHandler {

public:
	InputHandler(int size);

	int boardSize;
	bool positionSet;

	bool handleMouseClick(Board &board, int player, float x, float y);
};