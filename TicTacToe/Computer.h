#pragma once
#include <iostream>
#include "Board.h"

class Computer {

public:
	bool computerMove(Board &board, int player);
};
