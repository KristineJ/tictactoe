#pragma once
#include <iostream>
#include "SFML\Graphics.hpp"

const int SQUARE_SIZE = 200;
const int LINE_SIZE_GRID = 2;
const int MAX_BOARD_SIZE = 9;
const int SQUARE_OFFSET = 12;

class Board {

public:
	Board(int size);

	int boardHeight;
	int boardWidth;
	int boardSize;
	int squareSize;
	std::vector< std::vector<int> > matrix;

	void drawBoard(sf::RenderWindow &window);
	void setScore(int player, int row, int col, int xPos, int yPos);
	bool check_win(int player);
	void drawWinScene(sf::RenderWindow &window, int player);
	bool checkDraw();
	void drawEndScene(sf::RenderWindow &window);


private:
	int circleSpriteNumber;
	int crossSpriteNumber;

	sf::Texture crossTexture;
	sf::Texture circleTexture;

	sf::Text text;
	sf::Font font;

	std::vector <sf::Sprite> circleSprites;
	std::vector <sf::Sprite> crossSprites;

	void loadTextures(int size);
	void setSquareSize(int size);

};