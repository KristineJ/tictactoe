#include "Computer.h"

// COmputer chooses random free spot on the board
bool Computer::computerMove(Board &board, int player)
{
	int size = board.boardSize;
	int i = 0;
	int j = 0;
	int x = 0;
	int y = 0;

	// Give new random numbers until free spot is found
	while (board.matrix[i][j] != 0)
	{
		i = rand() % size;
		j = rand() % size;
	}

	// Choose spot if free
	if (board.matrix[i][j] == 0)
	{
		x = i*board.squareSize;
		y = j*board.squareSize;
		board.setScore(player, i, j, x, y);
		return true;
	}

	return false;
}